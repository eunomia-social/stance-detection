#!/bin/sh
# shellcheck disable=SC2001
text="The decision whether the UK should stay or leave the EU was put to a vote. With a vote of 52% to 48%, the UK has decided to leave the EU."
answer1="Indeed, British wish to leave EU."
answer2="No way, you are a troll"
answer3="I think this is a poor decision."
answer4="Is it final?"

data1="{\"text1\": \"${text}\", \"text2\": \"${answer1}\"}"
data2="{\"text1\": \"${text}\", \"text2\": \"${answer2}\"}"
data3="{\"text1\": \"${text}\", \"text2\": \"${answer3}\"}"
data4="{\"text1\": \"${text}\", \"text2\": \"${answer4}\"}"
data5="{\"invalid\": \"input text\"}"

# if not already, start the service:
# docker-compose up -d
# sleep 20
echo "Input,Output" > test_results.csv

response1=$(curl -sS --request POST --url http://localhost:5056/stance_detection --header 'Content-Type: application/json' --data "${data1}")
_data1="$(echo ${data1} | sed 's/,/;/g')"
echo "${_data1},${response1}" >> test_results.csv

response2=$(curl -sS --request POST --url http://localhost:5056/stance_detection --header 'Content-Type: application/json' --data "${data2}")
_data2="$(echo ${data2} | sed 's/,/;/g')"
echo "${_data2},${response2}" >> test_results.csv

response3=$(curl -sS --request POST --url http://localhost:5056/stance_detection --header 'Content-Type: application/json' --data "${data3}")
_data3="$(echo ${data3} | sed 's/,/;/g')"
echo "${_data3},${response3}" >> test_results.csv

response4=$(curl -sS --request POST --url http://localhost:5056/stance_detection --header 'Content-Type: application/json' --data "${data4}")
_data4="$(echo ${data4} | sed 's/,/;/g')"
echo "${_data4},${response4}" >> test_results.csv

# expected validation error
response5=$(curl -sS --request POST --url http://localhost:5056/stance_detection --header 'Content-Type: application/json' --data "${data5}" | sed 's/,/;/g')
_data5="$(echo ${data5} | sed 's/,/;/g')"
echo "${_data5},${response5}" >> test_results.csv
