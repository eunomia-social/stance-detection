# EUNOMIA Stance Detection
## Usage:
To get the stance detection results for two posts, a http POST request has to be made:
- url: http://stance-detection:5056/stance_detection
- json body: {"text1": "the text of post1", "text2": "the text of post2"}
The result of the analysis is returned in the json format:
- {"result": <stance_result>}<br />
where <stance_result> is one of: 
    - "support"
    - "deny"
    - "comment"
    - "query"
## Environment variables on integration with other components:
  - STANCE_DETECTION_PORT=5056
  - STANCE_DETECTION_WEB_CONCURRENCY=1 # test performance and change it if needed
## Other
- Simple test calls and results in [./tests](./tests) </br>
- Docker compose logs, docker-stats and curl responses in [./stats](./stats)