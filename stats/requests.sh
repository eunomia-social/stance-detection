#!/bin/bash
text1="$(curl -fsSL https://loripsum.net/api/1/long/plaintext)"
text2="$(curl -fsSL https://loripsum.net/api/1/long/plaintext)"
data="{\"text1\": \"${text1}\", \"text2\": \"${text2}\"}"
url="http://localhost:5056/stance_detection"
header="Content-Type: application/json"

mkdir -p output

mode=sequential
count=1024
concurrency=8

if [ "${1}" = "--count" ]; then
    shift
    count=${1}
    shift
fi
if [ "${1}" = "--parallel" ]; then
    shift
    mode=parallel
    if [ "${1}" = "--concurrency" ]; then
    shift
    concurrency=${1}
    shift
    fi
fi
start=1
end=$count
if [ "${1}" = "--append" ] && [ -f "output/${mode}.csv" ] ; then
    start=$(cat "output/${mode}.csv" | wc -l)
    end=$((start + count))
fi
shift "$#"
if [ "${start}" = "1" ]; then
    echo "Id,Total Time(s),Status Code" > "output/${mode}.csv"
fi
if [ "${mode}" = "sequential" ]; then
    seq ${start} ${end} | xargs -I "$" curl -sS -w "$,%{time_total},%{response_code}\n" -o /dev/null --request POST --url "${url}" --header "${header}" --data "${data}" >> "output/${mode}.csv"
else
    seq ${start} ${end} | xargs -I "$" -n1 -P${concurrency} curl -sS -w "$,%{time_total},%{response_code}\n" -o /dev/null --request POST --url "${url}" --header "${header}" --data "${data}" >> "output/${mode}.csv"
fi

# ctr-c to stop, to view the output, on a new terminal/tab: tail -f output/{sequential|parallel}.csv
exit 0
