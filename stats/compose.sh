#!/bin/bash
mkdir -p output
docker-compose -f ../docker-compose.yml logs -f --no-color >& output/compose.log
# ctr-c to stop, to view, on a new terminal/tab: tail -f output/compose.log
