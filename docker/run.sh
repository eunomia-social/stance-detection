#!/usr/bin/env bash

APP_ENV="${APP_ENV:-production}"
SUBJECTIVITY_PORT=${STANCE_DETECTION_PORT:-5051}
SUBJECTIVITY_WEB_CONCURRENCY=${STANCE_DETECTION_WEB_CONCURRENCY:-1}
export LRU_CACHE_CAPACITY=1
if [ "${APP_ENV}" = "development" ];then
  LD_PRELOAD=/usr/local/lib/libjemalloc.so uvicorn app.main:app --workers "$STANCE_DETECTION_WEB_CONCURRENCY" --host 0.0.0.0 --port "$STANCE_DETECTION_PORT" --reload
else
  LD_PRELOAD=/usr/local/lib/libjemalloc.so hypercorn --config file:/app/corn.py app.main:app
fi
