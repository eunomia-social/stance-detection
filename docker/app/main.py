"""-*- coding: utf-8 -*-."""
from fastapi import FastAPI, HTTPException
from fastapi.responses import ORJSONResponse, Response
from pydantic import BaseModel
import orjson
from .stance import stanceClassifier

app = FastAPI(redoc_url=None, docs_url=None, default_response_class=ORJSONResponse)


@app.head("/health", include_in_schema=False, status_code=204)
async def head_check():  # pragma: no cover
    """Health check."""
    return None


class RequestBody(BaseModel):
    """The Expected request body."""

    text1: str
    text2: str


@app.post("/stance_detection")
async def eunomia_stance_detection(post: RequestBody):
    """Stance detection.

    Returns one of:
    "support",
    "deny",
    "comment",
    "query",
    """
    try:
        result = stanceClassifier.compute_stance(text1=post.text1, text2=post.text2)
        data = orjson.dumps({"result": result}, option=orjson.OPT_SERIALIZE_NUMPY)
        return Response(content=data, media_type="application/json")
    except Exception as e:
        raise HTTPException(status_code=400, detail={"message": repr(e)})
