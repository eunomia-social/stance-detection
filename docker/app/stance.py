"""-*- coding: utf-8 -*-."""
import os
import sys
import torch
from transformers import (
    RobertaConfig,
    RobertaForSequenceClassification,
    RobertaTokenizer,
)
import numpy as np


class StanceDetection:
    """The detection process class."""

    def __init__(self, file_name="models/"):
        """Instance init."""
        self.output_dir = os.path.join(
            os.path.realpath(os.path.dirname(__file__)), file_name
        )
        self.output_model_file = os.path.join(self.output_dir, "pytorch_model.bin")
        self.output_config_file = os.path.join(self.output_dir, "config.json")
        self.model = self.initModel()
        self.tokenizer = RobertaTokenizer.from_pretrained("roberta-large")
        self.labels = {
            0: "support",
            1: "deny",
            2: "comment",
            3: "query",
        }  # define output labels

    def initModel(self):
        """Model init."""
        config = RobertaConfig.from_json_file(self.output_config_file)
        model = RobertaForSequenceClassification(config=config)
        state_dict = torch.load(
            self.output_model_file, map_location=torch.device("cpu")
        )
        model.load_state_dict(state_dict)

        model.eval()
        return model

    def compute_stance(self, text1="Hi", text2="Hello"):
        """Stance detection computation."""
        if text1 == "":
            text1 = " "
        if text2 == "":
            raise AssertionError("Text 2 is empty")

        # tokenize text
        inputs = self.tokenizer.encode_plus(
            text1,
            text2,
            add_special_tokens=True,
            max_length=96,
            return_token_type_ids=True,
            return_tensors="pt",
        )

        # evaluate
        self.model.eval()
        with torch.no_grad():
            outputs = self.model(**inputs)
        logits = outputs[:3]
        result = np.argmax(logits[0].detach().cpu().numpy(), 1)[0]

        return self.labels[result]


stanceClassifier = StanceDetection()

if __name__ == "__main__":
    stanceClassifier = StanceDetection()
    print(stanceClassifier.compute_stance(sys.argv[0], sys.argv[1]))
